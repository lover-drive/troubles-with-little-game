﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{

    public Vector3 speed;
    public float deathTimer = 1f;

    private float t;

    // Start is called before the first frame update
    void Start()
    {
        t = Time.time + deathTimer;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = transform.position + transform.TransformDirection(speed) * Time.deltaTime;

        if (Time.time > t) Destroy(gameObject);
    }
}
