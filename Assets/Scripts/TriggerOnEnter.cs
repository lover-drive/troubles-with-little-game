using System;
using UnityEngine;

public class TriggerOnEnter : Trigger {

    void OnTriggerEnter(Collider other) {
        onTrigger();
    }

}