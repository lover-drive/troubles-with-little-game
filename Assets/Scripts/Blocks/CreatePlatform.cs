﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatePlatform : Block {

    public VariableVector2 position;
    public VariableVector2 size;

    public Transform target;

    public ConnectorObject outResult;

    public VariableBool collisionEnabled;

    public Connector onTrigger;

    void Awake() {
         position.x.Set(target.position.x);
         position.y.Set(target.position.y);
         size.x.Set(target.localScale.x);
         size.y.Set(target.localScale.y);
    }

    void Start() {

    }

    // Update is called once per frame
    override public void Execute() {
        target.position = position.value;
        target.localScale = (Vector3)size.value + Vector3.forward;
        target.rotation = Quaternion.identity;
        if (target.GetComponent<Rigidbody>() != null) target.GetComponent<Rigidbody>().velocity = Vector3.zero;
        if (collisionEnabled) target.GetComponent<Collider>().enabled = collisionEnabled.value;

        if (outResult != null) outResult.obj = target;

        if (onTrigger && target.GetComponent<Trigger>()) target.GetComponent<Trigger>().onTrigger = () => {
            onTrigger.to.GetComponentInParent<Block>().DoExecute();
        };
    }

    override public void Disable() {
        target.gameObject.SetActive(false);
    }

    override public void Enable() {
        target.gameObject.SetActive(true);
    }
}
