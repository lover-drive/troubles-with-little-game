﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetTime : MonoBehaviour
{

    public ConnectorFloat result;

    void Update() {
        result.value = Restart.time;
    }

}
