﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CheckboxVariableBinder : MonoBehaviour
{

    public VariableBool variable;
    public Toggle field;

    // Start is called before the first frame update
    void Start()
    {
        field.isOn = variable.value;
    }

    // Update is called once per frame
    void Update()
    {
        variable.Set(field.isOn);
    }
}
