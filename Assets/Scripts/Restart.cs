using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Restart : MonoBehaviour {

    static public Restart instance;

    static public List<GameObject> destroyOnRestart = new List<GameObject>();

    static public float time => Time.time - lastRestart;

    static private float lastRestart;

    static public void DoRestart () {
        destroyOnRestart.Where(_ => _).ToList().ForEach(Destroy);
        EventStart.starts.ForEach(_ => _.DoExecute());

        lastRestart = Time.time;
    }

    public void RestartGame () {
        DoRestart();
    }

    void Start() {
        DoRestart();
    }
}