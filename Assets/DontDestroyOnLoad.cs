﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyOnLoad : MonoBehaviour
{

    static public Dictionary<string, bool> exists = new Dictionary<string, bool>();

    // Start is called before the first frame update
    void Start()
    {
        if (exists.ContainsKey(name)) {
            Destroy(gameObject);
            return;
        }
        exists[name] = true;
        GameObject.DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
