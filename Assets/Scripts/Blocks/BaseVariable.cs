﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseVariable : MonoBehaviour {
    virtual public void Set(string s) {

    }
    virtual public string Get() {
        return "";
    }

    virtual public bool isValid {
        get => true;
    }
}
