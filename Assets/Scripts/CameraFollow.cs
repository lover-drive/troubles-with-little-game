﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour {
    public Transform target;
    public float smoothTime = .25f;

    Vector3 v;

    void FixedUpdate() {
        if (target != null) {
            transform.position = Vector3.SmoothDamp(transform.position, new Vector3(target.position.x, target.position.y, transform.position.z), ref v, smoothTime);
        }
    }
}
