﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class BaseCondition : MonoBehaviour
{

    virtual public bool isFulfiled {
        get {
            return true;
        }
    }
}
