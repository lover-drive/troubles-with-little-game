﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ConnectorDrag : EventTrigger
{

    public Connector connector => GetComponent<Connector>();

    public override void OnPointerDown(PointerEventData eventData) {
        connector.OnPointerDown(eventData);
    }

    public override void OnPointerUp(PointerEventData eventData) {
        connector.OnPointerUp(eventData);
    }

    public override void OnPointerEnter(PointerEventData eventData) {
        connector.OnPointerEnter(eventData);
    }

    public override void OnPointerExit(PointerEventData eventData) {
        connector.OnPointerExit(eventData);
    }
}
