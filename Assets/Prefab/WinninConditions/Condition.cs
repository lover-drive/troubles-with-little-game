﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Condition<T> : BaseCondition
{

    public BaseVariable var;
    public T correctValue;

    override public bool isFulfiled {
        get {
            return var.Get() == correctValue.ToString();
        }
    }
}
