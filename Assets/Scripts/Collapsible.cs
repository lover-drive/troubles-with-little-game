﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

public class Collapsible : MonoBehaviour
{
    public RectTransform target;

    private Vector2 sizeDelta;

    private bool folded;

    public void Toggle () {
        if (!folded) {
            sizeDelta = target.sizeDelta;
            target.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 200f);
            target.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, 20f);

            target.GetComponentsInChildren<Canvas>()
                .ToList()
                .ForEach(_ => {
                    _.overrideSorting = false;
                });

            folded = true;
        } else {
            target.sizeDelta = sizeDelta;
            folded = false;

            target.GetComponentsInChildren<Canvas>()
                .ToList()
                .ForEach(_ => {
                    _.overrideSorting = true;
                });
        }
    }
}
