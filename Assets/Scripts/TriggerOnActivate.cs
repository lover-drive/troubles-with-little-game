using System;
using UnityEngine;

public class TriggerOnActivate : Trigger {

    private bool isActive;

    void Update() {
        if (isActive && Input.GetKeyDown(KeyCode.E)) onTrigger();
    }

    void OnTriggerEnter(Collider other) {
        isActive = true;
    }

    void OnTriggerExit(Collider other) {
        isActive = false;
    }

}