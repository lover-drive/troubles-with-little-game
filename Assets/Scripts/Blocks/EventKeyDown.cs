﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventKeyDown : Block {

    public VariableKeycode keycode;

    public override void Execute() {

    }

    // Start is called before the first frame update
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyDown(keycode.value)) DoExecute();
    }
}
