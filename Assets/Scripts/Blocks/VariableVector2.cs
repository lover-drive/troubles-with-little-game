﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariableVector2 : Variable<Vector2>
{

    public VariableFloat x;
    public VariableFloat y;


    public override string Get() {
        return value.ToString();
    }

    public override void Set(string s) {
        // value = float.Parse(s);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (x == null || y == null) return;
        m_value = new Vector2(x.value, y.value);
    }
}
