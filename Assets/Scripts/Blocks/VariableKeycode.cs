﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariableKeycode : Variable<string>
{

    public bool isPressed {
        get {
            try {
                return Input.GetKey(value);
            } catch {
                return false;
            }
        }
    }
    public bool wasDown {
        get {
            try {
                return Input.GetKeyDown(value);
            } catch {
                return false;
            }
        }
    }
    public bool wasUp {
        get {
            try {
                return Input.GetKeyUp(value);
            } catch {
                return false;
            }
        }
    }

    public void Set2 (string s) {
        m_value = s;
    }

    override public bool isValid => value != "";
}
