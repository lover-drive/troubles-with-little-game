﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Dragable : EventTrigger
{

    static public bool canBeDragged = true;

    private bool dragging;
    private Vector2 offset;

    public void Update() {
        if (dragging) {
            transform.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y) + offset;
        }
    }

    public override void OnPointerDown(PointerEventData eventData) {
        if (!canBeDragged) return;

        dragging = true;
        offset = (Vector2)transform.position - new Vector2(Input.mousePosition.x, Input.mousePosition.y);
    }

    public override void OnPointerUp(PointerEventData eventData) {
        dragging = false;
    }
}
