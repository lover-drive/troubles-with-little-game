﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariableBool : Variable<bool> {

    public bool isChecked;

    public override string Get() {
        return value.ToString();
    }

    public override void Set(string s) {
        isChecked = bool.Parse(s);
    }
}
