﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour
{

    public Text text;

    public int amount;
    public int fixedAmount;

    // Start is called before the first frame update
    void Start()
    {
        amount = GameObject.FindObjectsOfType<BaseCondition>().Length;
        
    }

    // Update is called once per frame
    void Update()
    {
        fixedAmount = GameObject.FindObjectsOfType<BaseCondition>().Where(_ => _.isFulfiled).Count();
        text.text = "Bugs fixed: " + fixedAmount.ToString() + " / " + amount.ToString();
    }
}
