﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariableFloat : Variable<float> {

    public override string Get() {
        return value.ToString();
    }

    public override void Set(string s) {
        m_value = float.Parse(s);
    }
}
