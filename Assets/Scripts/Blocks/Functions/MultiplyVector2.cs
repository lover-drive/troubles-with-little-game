﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MultiplyVector2 : MonoBehaviour
{

    public VariableVector2 a;
    public VariableFloat b;
    public ConnectorVector2 result;

    void Update() {
        if (a == null) return;
        if (b == null) return;
        result.value = a.value * b.value;
    }

}
