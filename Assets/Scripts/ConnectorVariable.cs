﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public abstract class ConnectorVariable<T> : ConnectorObject
{
    public T value;


    public float priority;

    private ConnectorVariable<T> toTyped => to as ConnectorVariable<T>;

    override public void Update() {
        base.Update();

        if (toTyped == null) return;
        if (toTyped.priority < priority) {
            toTyped.value = value;
        }
    }
}
