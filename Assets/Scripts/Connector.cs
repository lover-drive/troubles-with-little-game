﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Connector : MonoBehaviour
{

    static public Connector currentConnectorTarget;

    public RectTransform line;

    public Connector to;

    public RectTransform rectTransform => GetComponent<RectTransform>();

    private bool dragging;

    // Start is called before the first frame update
    void Start()
    {
        
        if (to != null) to.to = this;
    }

    public void DrawTo (Vector3 position) {
        float angle = Mathf.Atan2(rectTransform.position.y - position.y, rectTransform.position.x - position.x) * Mathf.Rad2Deg + 180;
        line.rotation = Quaternion.Euler(Vector3.forward * angle);
        line.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Vector3.Distance(rectTransform.position, position));
    }

    // Update is called once per frame
    virtual public void Update()
    {
        if (to != null) {
            if (to.to != this) to = null;
        }
        if (dragging) {
            DrawTo(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0));
            return;
        }

        if (to == null) {
            line.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 0f);
            return;
        }

        float angle = Mathf.Atan2(rectTransform.position.y - to.rectTransform.position.y, rectTransform.position.x - to.rectTransform.position.x) * Mathf.Rad2Deg + 180;
        line.rotation = Quaternion.Euler(Vector3.forward * angle);
        line.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Vector3.Distance(rectTransform.position, to.rectTransform.position));

    }

    static public void Connect (Connector a, Connector b) {
        if (a != null) a.to = b;
        if (b != null) b.to = a;

        if (a != null) a.line.gameObject.SetActive(true);
        if (b != null) b.line.gameObject.SetActive(false);
    }

    public void OnPointerDown(PointerEventData eventData) {
        dragging = true;
    }

    public void OnPointerUp(PointerEventData eventData) {
        dragging = false;

        Connect(this, currentConnectorTarget);
    }

    public void OnPointerEnter(PointerEventData eventData) {
        currentConnectorTarget = this;
        Dragable.canBeDragged = false;
    }

    public void OnPointerExit(PointerEventData eventData) {
        currentConnectorTarget = null;
        Dragable.canBeDragged = true;

        if (dragging) {
            if (to != null) to.to = null;
            to = null;
        }
    }
}
