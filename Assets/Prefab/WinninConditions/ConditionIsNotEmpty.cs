﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionIsNotEmpty : BaseCondition
{

    public BaseVariable var;

    override public bool isFulfiled {
        get {
            return var.isValid;
        }
    }

}
