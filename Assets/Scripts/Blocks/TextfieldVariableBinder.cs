﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextfieldVariableBinder : MonoBehaviour
{

    public BaseVariable variable;
    public InputField field;

    // Start is called before the first frame update
    void Start()
    {
        if (variable == null) variable = GetComponent<BaseVariable>();
        field.text = variable.Get();
    }

    // Update is called once per frame
    void Update()
    {
        variable.Set(field.text);

        var k = (variable as VariableKeycode);
        if (k != null) {
            k.Set2(field.text);
        }
    }
}
