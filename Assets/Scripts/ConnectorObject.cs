﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ConnectorObject : Connector
{
    public Transform obj;

    override public void Update () {
        base.Update();
        var con = (to as ConnectorObject);
        if (to == null) return;
        if (obj == null && con.obj != null) obj = con.obj;
        else if (obj != null && con.obj == null) con.obj = obj;
    }
}
