﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionConnectorNotEmpty : BaseCondition
{

    public Connector connector;

    override public bool isFulfiled {
        get {
            return connector.to != null;
        }
    }
}
