﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Divide : MonoBehaviour
{

    public VariableFloat a;
    public VariableFloat b;
    public ConnectorFloat result;

    void Update() {
        if (a == null) return;
        if (b == null) return;
        result.value = a.value / b.value;
    }

}
