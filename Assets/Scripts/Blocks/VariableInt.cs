﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariableInt : Variable<int> {

    public override string Get() {
        return value.ToString();
    }

    public override void Set(string s) {
        m_value = int.Parse(s);
    }
}
