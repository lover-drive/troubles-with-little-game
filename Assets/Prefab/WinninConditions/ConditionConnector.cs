﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionConnector : BaseCondition
{

    public Connector connector;
    public Connector correctTo;

    override public bool isFulfiled {
        get {
            return connector.to == correctTo;
        }
    }
}
