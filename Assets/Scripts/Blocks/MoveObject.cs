﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveObject : Block {

    public VariableVector2 speed;

    public ConnectorObject target;

    // Update is called once per frame
    override public void Execute() {
        if (target.to == null) return;
        if ((target.to as ConnectorObject).obj == null) return;
        (target.to as ConnectorObject).obj.transform.position += (Vector3)speed.value * Time.deltaTime;
    }

    override public void Disable() {
        // target.gameObject.SetActive(false);
    }

    override public void Enable() {
        // target.gameObject.SetActive(true);
    }
}
