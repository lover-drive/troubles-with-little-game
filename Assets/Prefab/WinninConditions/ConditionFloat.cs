﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConditionFloat : Condition<float>
{

    public float min;
    public float max;

    override public bool isFulfiled {
        get {
            var v = (var as VariableFloat);
            return v.value >= min && v.value <= max;
        }
    }
}
