﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class FileSelector : MonoBehaviour, IPointerClickHandler
{

    static FileSelector selector;

    public CanvasGroup window;

    public void OnPointerClick(PointerEventData eventData) {
        if (selector) {
            selector.window.alpha = 0f;
            selector.window.blocksRaycasts = false;
        }
        if (selector == this) {
            selector = null;
        } else {
            selector = this;
            selector.window.alpha = 1f;
            selector.window.blocksRaycasts = true;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        window.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
