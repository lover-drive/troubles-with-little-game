using System;
using UnityEngine;

public  class TriggerTouchToRestart : MonoBehaviour {

    public bool killsEnemies;

    void OnTriggerEnter(Collider other) {
        if (other.tag == "Player") {
            Restart.DoRestart();
            return;
        }

        if (killsEnemies) {
            Destroy(other.gameObject);
        }
    }
}