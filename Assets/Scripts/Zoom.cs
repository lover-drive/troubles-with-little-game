﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Zoom : EventTrigger
{

    private bool isZooming;

    public CanvasScaler scaler => GetComponent<CanvasScaler>();

    override public void OnPointerEnter(PointerEventData eventData) {
        isZooming = true;
    }

    override public void OnPointerExit(PointerEventData eventData) {
        isZooming = false;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (!isZooming) return;

        scaler.scaleFactor += Input.mouseScrollDelta.y * Time.deltaTime * 3f;
    }
}
