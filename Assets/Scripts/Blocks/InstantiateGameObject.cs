﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateGameObject : Block {

    public VariableVector2 position;
    public VariableVector2 size;

    public GameObject target;

    public ConnectorObject outResult;

    void Awake() {
        position.x.Set(target.transform.position.x);
        position.y.Set(target.transform.position.y);
        size.x.Set(target.transform.localScale.x);
        size.y.Set(target.transform.localScale.y);
    }

    void Start() {

    }

    // Update is called once per frame
    override public void Execute() {
        var newGo = GameObject.Instantiate(target, position.value, Quaternion.identity);
        newGo.SetActive(true);
        newGo.transform.position = position.value;
        newGo.transform.localScale = (Vector3)size.value + Vector3.forward;
        Restart.destroyOnRestart.Add(newGo.gameObject);

        if (outResult != null) outResult.obj = target.transform;
    }

    override public void Disable() {
        target.gameObject.SetActive(false);
    }

    override public void Enable() {
        target.gameObject.SetActive(true);
    }
}
