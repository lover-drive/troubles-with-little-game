﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Block : MonoBehaviour
{
    public Connector input;
    public Connector output;

    public Block next => output.to.GetComponentInParent<Block>();

    abstract public void Execute ();

    virtual public void Disable () {
        
    }

    virtual public void Enable () {

    }
    
    public void CheckIfEnabled () {
        if (input == null) return;

        if (input.to == null) this.Disable();
    }

    virtual public void DoExecute () {
        Enable();
        Execute();

        Debug.Log(name);
        if (output == null) return;
        if (output.to == null) return;
        if (next == null) return;
        next.DoExecute();
    }
}
