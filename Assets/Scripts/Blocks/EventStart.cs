﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EventStart : Block
{

    static public List<EventStart> starts = new List<EventStart>();

    public override void Execute() {
        GameObject.FindObjectsOfType<Block>()
            .ToList()
            .ForEach(_ => {
                _.CheckIfEnabled();
            });
    }

    // Start is called before the first frame update
    public void Start()
    {
        DoExecute();
        starts.Add(this);
    }

    void OnDestroy() {
        starts.Remove(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
