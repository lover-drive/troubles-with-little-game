﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Variable<T> : BaseVariable {

    public T value { 
        get {
            ConnectorVariable<T> con = connector as ConnectorVariable<T>;
            if (con != null) {
                if (con.to != null) {
                    Debug.Log(con.value);
                    return con.value;
                }
            }
            return m_value;
        }
    }

    virtual public void Set (T newValue) {
        m_value = newValue;
    }

    override public string Get () {
        return value.ToString();
    }

    public ConnectorObject connector;
    
    public T m_value;

    override public bool isValid => m_value != null;
}
