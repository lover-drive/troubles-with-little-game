﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gravity : Block {

    public VariableVector2 g;

    // Update is called once per frame
    override public void Execute() {
        Physics.gravity = (Vector3)g.value;
    }

    override public void Disable() {
        // target.gameObject.SetActive(false);
    }

    override public void Enable() {
        // target.gameObject.SetActive(true);
    }
}
