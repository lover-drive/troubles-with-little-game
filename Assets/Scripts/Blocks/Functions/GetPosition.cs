﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetPosition : MonoBehaviour
{

    public ConnectorObject target;
    public ConnectorVector2 result;

    void Update() {
        if (target.obj == null) return;
        result.value = target.obj.transform.position;
    }

}
