﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : Block
{

    public Rigidbody rb;
    public Animator animator;

    [Header("Input")]
    public VariableKeycode left;
    public VariableKeycode right;
    public VariableKeycode jump;

    public VariableFloat speed;
    public VariableFloat jumpHeiht;

    public VariableVector2 position;

    public ConnectorObject outResult;


    public bool isGrounded => Physics.Raycast(rb.transform.position, Vector3.down, 1f);

    void Awake() {
        position.x.Set(rb.transform.position.x);
        position.y.Set(rb.transform.position.y);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    override public void Enable() {
        rb.gameObject.SetActive(true);
    }

    override public void Disable() {
        rb.gameObject.SetActive(false);
    }

    override public void Execute() {
        rb.transform.position = position.value;
        rb.velocity = Vector3.zero;
        if (outResult != null) outResult.obj = rb.transform;
    }

    // Update is called once per frame
    public void Update()
    {
        Vector3 v = Vector3.up * rb.velocity.y;
        bool moving = false;

        if (left.isPressed) {
            v = v + Vector3.left * speed.value;
            moving = true;
        }
        if (right.isPressed) {
            v = v + Vector3.right * speed.value;
            moving = true;
        }
        if (jump.wasDown && isGrounded) {
            // rb.AddForce(Vector3.up * jumpHeiht.value * Time.deltaTime, ForceMode.VelocityChange);
            v = v + Vector3.up * jumpHeiht.value;
        }
        rb.velocity = v;

        animator.SetBool("walking", moving);
        animator.SetFloat("direction", v.x < 0 ? 1f : 0f);
    }
}
