﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sin : MonoBehaviour
{

    public VariableFloat t;
    public ConnectorFloat result;

    void Update() {
        if (t == null) return;
        result.value = Mathf.Sin(t.value);
    }

}
