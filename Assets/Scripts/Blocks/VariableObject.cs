﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VariableObject : Variable<GameObject> {

    public override string Get() {
        return value.ToString();
    }

    public override void Set(string s) {
        // value = float.Parse(s);
    }
}
