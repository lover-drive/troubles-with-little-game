using System;
using UnityEngine;

public abstract class Trigger : MonoBehaviour {
    public Action onTrigger = () => {};
}